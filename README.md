# Virtual wallet
Virtual wallet project
Project Description

Virtual Wallet is a web application that enables you to continently manage your budget. Every user can send and receive money (user to user) and put money in his Virtual Wallet (bank to app). Virtual Wallet has a core set of requirements that are absolute must and a variety of optional features.

Functional Requirements

Entities

• Each user have a username, password, email, phone number, credit/debit card and a photo. o Username must be unique and between 2 and 20 symbols.

o Password must be at least 8 symbols and should contain capital letter, digit and special symbol (+, -, *, &, ^, …)

o Email must be valid email and unique in the system.

o Phone number must be 10 digits and unique in the system.

• Credit/debit card have a number, expiration date, card holder and a check number o Card number must be unique and with 16 digits. o Card holder must be between 2 and 30 symbols. o Check number must be 3 digits.

Public Part

The public part is accessible without authentication i.e., for anonymous users.

Anonymous users are able to register and login.

Anonymous users see detailed information about Virtual Wallet and its features.

Private part

Accessible only if the user is authenticated.

Users are able to login/logout, update their profile, manage their credit/debit card, make transfers to other users, and view the history of their transfers.

Users are able to view and edit their profile information, except their username, which is selected on registration and cannot be changed afterwards. The required fields for registration are username, email, and phone number.

Each user is able to register one credit or debit card, which is used to transfer money into their Virtual Wallet.

Note: The transfer is done by a separate dummy REST API. It must provide a single endpoint for money withdraw from the credit/debit card. It confirms transfers on random basis (it is dummy, right).

Note: DO NOT use actual credit card information!

Users is able to transfer money to other users by entering another user's phone number, username or email and desired amount to be transferred. Users can search by phone number, username, or email to select the recipient user for the transfer, but when viewing recipient users only username must be displayed.

Each transfer must go through confirmation step which displays the transfer details and allows either confirming it or editing it.

Users are able to view a list of their transactions filtered by period, recipient, and direction (incoming or outgoing) and sort them by amount and date. Transaction list support pagination.

Administrative part

Accessible to users with administrative privileges.

Admin users is able to see list of all users and search them by phone number, username or email and block or unblock them. User list support pagination. A blocked user is able to do everything as normal user, except to make transactions.

Admin users is able to view a list of all user transactions filtered by period, sender, recipient, and direction (incoming or outgoing) and sort them by amount and date. Transaction list support pagination.

Optional feature

Email Verification – In order for the registration to be completed, the user must verify their email by clicking on a link send to their email by the application. Before verifying their email, users cannot make transactions.

REST API

To provide other developers with your service, you need to develop a REST API. It should leverage HTTP as a transport protocol and clear text JSON for the request and response payloads.

A great API is nothing without a great documentation. The documentation holds the information that is required to successfully consume and integrate with an API.

The REST API provides the following capabilities:

1. Users

• CRUD Operations

• Add/view/update/delete credit/debit card

• Block/unblock user

• Search by username, email, or phone

2. Transactions

• Add money to wallet

• Make transaction

• List transactions

• Filter by date, sender, recipient, and direction (in/out)

• Sort by date or amount 

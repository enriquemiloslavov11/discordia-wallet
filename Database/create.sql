create table wallet.roles
(
    id int auto_increment,
    name varchar(10) not null,
    constraint roles_id_uindex
        unique (id)
);

alter table wallet.roles
    add primary key (id);

create table wallet.users
(
    id int auto_increment,
    username varchar(20) not null,
    hashed_password varchar(255) not null,
    email varchar(30) not null,
    phone_number varchar(10) not null,
    status enum('BLOCK', 'UNBLOCK') not null,
    user_photo varchar(1024) null,
    role_id int not null,
    enabled tinyint(1) not null,
    salt varchar(1024) null,
    constraint users_email_uindex
        unique (email),
    constraint users_id_uindex
        unique (id),
    constraint users_phone_number_uindex
        unique (phone_number),
    constraint users_username_uindex
        unique (username),
    constraint users_roles_fk
        foreign key (role_id) references wallet.roles (id)
);

alter table wallet.users
    add primary key (id);

create table wallet.cards
(
    id int auto_increment,
    card_number varchar(16) not null,
    card_holder varchar(30) not null,
    check_number varchar(3) not null,
    expiration_date date not null,
    user_id int not null,
    card_type enum('DEBIT', 'CREDIT') not null,
    constraint cards_id_uindex
        unique (id),
    constraint cards__users_fk
        foreign key (user_id) references wallet.users (id)
);

alter table wallet.cards
    add primary key (id);

create table wallet.confirmation_tokens
(
    id int auto_increment,
    confirmation_token varchar(255) null,
    created_time datetime null,
    user_id int null,
    constraint confirmation_tokens_id_uindex
        unique (id),
    constraint confirmation_tokens_users_id_fk
        foreign key (user_id) references wallet.users (id)
);

alter table wallet.confirmation_tokens
    add primary key (id);

create table wallet.pending_transactions
(
    id int auto_increment,
    sender_id int null,
    recipient_id int null,
    amount double(69,2) not null,
    confirmation_token_id int null,
    transaction_date date not null,
    constraint pending_transactions_id_uindex
        unique (id),
    constraint pending_transactions_confirmation_tokens_id_fk
        foreign key (confirmation_token_id) references wallet.confirmation_tokens (id),
    constraint pending_transactions_users_id_fk
        foreign key (recipient_id) references wallet.users (id),
    constraint pending_transactions_users_id_fk_2
        foreign key (sender_id) references wallet.users (id)
);

alter table wallet.pending_transactions
    add primary key (id);

create table wallet.transactions
(
    id int auto_increment,
    transaction_date date not null,
    amount double(64,2) not null,
    sender_id int not null,
    recipient_id int not null,
    constraint transactions_id_uindex
        unique (id),
    constraint transactions__recipient_users_fk
        foreign key (recipient_id) references wallet.users (id),
    constraint transactions__users_fk
        foreign key (sender_id) references wallet.users (id)
);

alter table wallet.transactions
    add primary key (id);

create table wallet.wallets
(
    id int auto_increment,
    amount double(64,2) not null,
    user_id int not null,
    constraint wallets_id_uindex
        unique (id),
    constraint wallets__users_fk
        foreign key (user_id) references wallet.users (id)
);

alter table wallet.wallets
    add primary key (id);


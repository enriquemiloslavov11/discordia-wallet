package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.repositories.interfaces.UserRepository;
import com.telerikacademy.virtualwallet.services.implementations.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
//@MockitoSettings(strictness = Strictness.LENIENT)
public class UserServiceImplTests {
    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;
//    @Test
//    public void create_should_callRepository_when_userWithSameNameDoesNotExist() {
//        // Arrange
//        User mockUser = Helpers.createMockUser();
//
//        Mockito.when(mockRepository.getByField("username",mockUser.getUsername()))
//                .thenThrow(EntityNotFoundException.class);
//
//        // Act
//        service.create(mockUser);
//
//        // Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .create(mockUser);
//    }


    @Test
    public void create_should_throw_when_userWithSameUsernameExists() {
        // Arrange
        User mockUser = Helpers.createMockUser();

        Mockito.lenient().when(mockRepository.getByField("username",mockUser.getUsername()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockUser));
    }
    @Test
    void getAll_should_callRepository_when_userIsAdmin() {
        // Arrange
        User mockAdmin = Helpers.createMockAdmin();

        // Act
        service.getAll(mockAdmin);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }
    @Test
    void getAll_should_throwException_when_initiatorIsNotAdmin() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        User mockInitiator = Helpers.createMockAdmin();
        mockInitiator.setUsername("MockInitiator");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAll(mockUser));
    }
    @Test
    void getById_should_throwException_when_initiatorIsNotOwner() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        User mockInitiator = Helpers.createMockUser();
        mockInitiator.setId(2);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getById(mockUser.getId(), mockInitiator));
    }
    @Test
    public void getById_should_returnUser_when_matchExist() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        User mockInitiator = Helpers.createMockUser();
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);
        // Act
        User result = service.getById(mockUser.getId(), mockInitiator);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getPhoneNumber(), result.getPhoneNumber())
        );
    }
    @Test
    public void create_should_throw_when_userWithSamePhoneExists() {
        // Arrange
        User mockUser = Helpers.createMockUser();

        Mockito.lenient().when(mockRepository.getByField("phone_number", mockUser.getPhoneNumber()))
                .thenReturn(mockUser);
        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockUser));
    }
    @Test
    public void create_should_throw_when_userWithSameEmailExists() {
       // Arrange
        User mockUser = Helpers.createMockUser();

       Mockito.lenient().when(mockRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockUser));
   }
    @Test
    void setStatus_should_callRepository_when_userIsAdmin() {
        // Arrange
        User mockUser=Helpers.createMockUser();
        User mockAdmin = Helpers.createMockAdmin();

        // Act
        service.setStatus(mockUser,mockAdmin);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }
    @Test
    void getById_should_throwException_when_initiatorIsNotAdmin() {
        // Arrange
        User mockUser=Helpers.createMockUser();
        User mockInitiator = Helpers.createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.setStatus(mockUser, mockInitiator));
    }
    @Test
    void delete_should_callRepository_when_initiatorIsAdmin() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        User mockInitiator = Helpers.createMockAdmin();
        mockInitiator.setUsername("MockInitiator");

        // Act
        service.delete(mockUser.getId(), mockInitiator);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockUser.getId());
    }
    @Test
    void delete_should_throwException_when_initiatorIsNotAdmin() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        User mockInitiator = Helpers.createMockUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockUser.getId(), mockInitiator));
    }
    @Test
    void delete_should_callRepository_when_matchExist() {
        // Arrange

        User mockUser = Helpers.createMockUser();
        User mockInitiator = Helpers.createMockAdmin();

        // Act
        service.delete(mockUser.getId(),mockInitiator);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockUser.getId());
    }
    @Test
    void search_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.searchByKeyword(Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.searchByKeyword(Optional.empty());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .searchByKeyword(Optional.empty());
    }


}

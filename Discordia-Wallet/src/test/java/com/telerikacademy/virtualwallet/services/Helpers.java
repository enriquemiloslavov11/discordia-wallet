package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.entities.*;
import com.telerikacademy.virtualwallet.entities.enums.CardType;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Helpers {
    public static User createMockUser() {
        return createMockUser("User", 2);
    }
    public static User createMockSender() {
        return createMockSender("User", 2);
    }
    public static User createMockRecipient() {
        return createMockRecipient("User", 2);
    }

    public static User createMockAdmin() {
        return createMockUser("Admin", 1);
    }

    private static User createMockUser(String role, int id) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        mockUser.setPhoneNumber("MockPhoneNumber");
        mockUser.setRole(createMockRole(role, id));
        return mockUser;
    }
    private static User createMockSender(String role, int id) {
        var mockUser = new User();
        mockUser.setId(2);
        mockUser.setEmail("mock@sender.com");
        mockUser.setUsername("MockSenderUsername");
        mockUser.setPassword("MockSenderPassword");
        mockUser.setPhoneNumber("MockSenderPhoneNumber");
        mockUser.setRole(createMockRole(role, id));
        return mockUser;
    }
    private static User createMockRecipient(String role, int id) {
        var mockUser = new User();
        mockUser.setId(3);
        mockUser.setEmail("mock@recipient.com");
        mockUser.setUsername("MockRecipientUsername");
        mockUser.setPassword("MockRecipientPassword");
        mockUser.setPhoneNumber("MockRecipientPhoneNumber");
        mockUser.setRole(createMockRole(role, id));
        return mockUser;
    }
    public static Role createMockRole(String role, int id) {
        var mockRole = new Role();
        mockRole.setId(id);
        mockRole.setName(role);
        return mockRole;
    }
    public static Card createMockCard() {
        var mockCard = new Card();
        mockCard.setId(1);
        mockCard.setCardNumber("1482387407517345");
        mockCard.setExpirationDate(LocalDate.now());
        mockCard.setCardHolder("MockUser");
        mockCard.setCheckNumber("234");
        mockCard.setCardType(CardType.DEBIT);
        mockCard.setUser(createMockUser());
        return mockCard;
    }


    public static Transaction createMockTransaction() {
        var mockTransaction = new Transaction();
        mockTransaction.setId(1);
        mockTransaction.setTransactionDate(LocalDate.now());
        mockTransaction.setAmount(new BigDecimal(3400));
        mockTransaction.setSender(createMockSender());
        mockTransaction.setRecipient(createMockRecipient());
        return mockTransaction;
    }

    public static Wallet createMockWallet() {
        var mockWallet = new Wallet();
        mockWallet.setId(1);
        mockWallet.setBalance(new BigDecimal(700));
        mockWallet.setUser(createMockUser());
        return mockWallet;
    }
}

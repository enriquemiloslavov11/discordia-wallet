package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.entities.Role;
import com.telerikacademy.virtualwallet.repositories.interfaces.RoleRepository;
import com.telerikacademy.virtualwallet.services.implementations.RoleServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class RoleServiceImplTests {
    @Mock
    RoleRepository mockRepository;

    @InjectMocks
    RoleServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnRole_when_matchExist() {
        // Arrange
        Role mockRole = Helpers.createMockRole("Test", 1);
        Mockito.when(mockRepository.getById(mockRole.getId()))
                .thenReturn(mockRole);
        // Act
        Role result = service.getById(mockRole.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRole.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRole.getName(), result.getName())
        );
    }

}


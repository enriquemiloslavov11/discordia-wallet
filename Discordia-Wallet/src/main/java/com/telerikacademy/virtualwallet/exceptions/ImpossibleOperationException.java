package com.telerikacademy.virtualwallet.exceptions;

public class ImpossibleOperationException extends RuntimeException {

    public ImpossibleOperationException(String type, int id, String field) {
        super(String.format("%s with id: %d already has a %s",type, id, field));
    }

}

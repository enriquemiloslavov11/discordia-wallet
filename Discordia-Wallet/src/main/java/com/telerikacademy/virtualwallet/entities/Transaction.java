package com.telerikacademy.virtualwallet.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor

@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private User sender;

    @ManyToOne
    @JoinColumn(name = "recipient_id")
    private User recipient;

    @DateTimeFormat(pattern ="yyyy-MM-dd")
    @Column(name = "transaction_date")
    private LocalDate transactionDate;

    @Column(name = "amount")
    private BigDecimal amount;

    public Transaction(int id, User sender, User recipient, LocalDate transactionDate, BigDecimal amount) {
        this.id = id;
        this.sender = sender;
        this.recipient = recipient;
        this.transactionDate = transactionDate;
        this.amount = amount;
    }

}

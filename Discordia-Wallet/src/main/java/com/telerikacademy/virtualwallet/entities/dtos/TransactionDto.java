package com.telerikacademy.virtualwallet.entities.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class TransactionDto {

    @NotEmpty(message = "recipient cannot be empty")
    private String recipient;

   @Positive(message = "Balance should be positive.")
    private BigDecimal balance;
}

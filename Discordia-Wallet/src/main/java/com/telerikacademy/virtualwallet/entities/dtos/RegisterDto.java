package com.telerikacademy.virtualwallet.entities.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class RegisterDto extends LoginDto{

    @Size(min = 8, message = "Password confirmation must be 8 characters or more")
    @NotEmpty(message = "Password confirmation can't be empty")
    private String passwordConfirm;

    @Size(max=30, message = "Email must not be over 30 characters")
    @NotEmpty(message = "Email can't be empty")
    private String email;

    @Pattern(regexp="\\d{10}",message = "Phone number must be exactly 10 digits")//10 digits
    @NotEmpty(message = "Phone number can't be empty")
    private String phoneNumber;
}

package com.telerikacademy.virtualwallet.entities.dtos;

import com.telerikacademy.virtualwallet.entities.enums.Direction;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class UserTransactionDto {

    @Positive(message = "ID should be positive.")
    private int id;

    @PositiveOrZero(message = "SenderID should be positive or zero.")
    private String sender;

    @PositiveOrZero(message = "RecipientID should be positive or zero.")
    private String recipient;

    @DateTimeFormat(pattern ="yyyy-MM-dd")
    private LocalDate transactionDate;

    @Positive(message = "Balance should be positive.")
    private BigDecimal balance;

    @Enumerated(EnumType.STRING)
    private Direction direction;
}

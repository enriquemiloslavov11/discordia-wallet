package com.telerikacademy.virtualwallet.entities.enums;

public enum Status {
    UNBLOCK,BLOCK;

    public String toString() {
        switch (this) {
            case BLOCK:
                return "blocked";
            case UNBLOCK:
                return "unblocked";
            default:
                return "";
        }
    }
}

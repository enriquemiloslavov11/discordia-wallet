package com.telerikacademy.virtualwallet.entities.enums;

public enum Direction {
    INCOMING,
    OUTGOING;

    public String toString() {
        switch (this) {
            case INCOMING:
                return "incoming";
            case OUTGOING:
                return "outgoing";
            default:
                return "";
        }
    }
}

package com.telerikacademy.virtualwallet.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.virtualwallet.entities.enums.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "username")
    private String username;

    @JsonIgnore
    @Column(name = "hashed_password")
    private String hashedPassword;

    @Email
    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "enabled")
    private boolean isVerified;

    @Column(name = "user_photo")
    private String userPhoto;

    @JsonIgnore
    @Column(name ="salt")
    private String salt;

    public boolean isUser(String user) {
        return this.getRole().getName().equalsIgnoreCase(user);
    }

    public boolean isAdmin() {
        return this.getRole().getName().equalsIgnoreCase("admin");
    }

}

package com.telerikacademy.virtualwallet.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "pending_transactions")

public class PendingTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private User sender;

    @ManyToOne
    @JoinColumn(name = "recipient_id")
    private User recipient;

    @Column(name = "amount")
    private BigDecimal amount;

    @DateTimeFormat(pattern ="yyyy-MM-dd")
    @Column(name = "transaction_date")
    private LocalDate transactionDate;

    @OneToOne
    @JoinColumn(name = "confirmation_token_id")
    private ConfirmationToken token;

}

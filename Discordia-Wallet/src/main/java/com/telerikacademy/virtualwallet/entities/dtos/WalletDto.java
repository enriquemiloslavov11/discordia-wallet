package com.telerikacademy.virtualwallet.entities.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class WalletDto {

    @PositiveOrZero(message = "Balance should be positive or zero.")
    private BigDecimal balance;

    @PositiveOrZero(message = "UserId should be positive or zero.")
    private int userId;
}

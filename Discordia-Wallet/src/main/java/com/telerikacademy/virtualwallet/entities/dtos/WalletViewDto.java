package com.telerikacademy.virtualwallet.entities.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class WalletViewDto {

    @PositiveOrZero(message = "Balance should be positive or zero.")
    private BigDecimal balance;

    @NotEmpty(message = "UserName should not be empty.")
    private String username;
}

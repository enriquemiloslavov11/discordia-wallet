package com.telerikacademy.virtualwallet.entities.dtos;

import com.telerikacademy.virtualwallet.entities.enums.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class UserDto{

    @Size(min = 3,max=20, message = "Username must be between 3 and 20 characters")
    @NotBlank(message = "username can't be empty")
    private String username;

    @NotBlank(message = "Password can't be empty")
    @Size(min = 8, message = "Password must be 8 characters or more")
    private String password;

    @NotBlank(message = "confirm password can't be empty")
    @Size(min = 8, message = "Confirm password must be 8 characters or more")
    private String confirmPassword;

    @NotBlank(message = "Email can't be empty")
    @Size(max=30, message = "Email must not be over 30 characters")
    @Email(message = "Valid email required")
    private String email;

    @NotBlank(message = "Phone number can't be empty")
    @Pattern(regexp="\\d{10}",message = "Phone number must be exactly 10 digits")//10 digits
    private String phoneNumber;

    private Status status;

    private String userPhoto;
}

package com.telerikacademy.virtualwallet.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.virtualwallet.entities.enums.CardType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "cards")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "card_number")
    private String cardNumber;

    @DateTimeFormat(pattern ="yyyy-MM-dd")
    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    @Column(name = "card_holder")
    private String cardHolder;

    @Column(name = "check_number")
    private String checkNumber;

    @OneToOne
    @JsonIgnore
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "card_type")
    @Enumerated(EnumType.STRING)
    private CardType cardType;
}

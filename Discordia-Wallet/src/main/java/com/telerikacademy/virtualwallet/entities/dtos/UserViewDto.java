package com.telerikacademy.virtualwallet.entities.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserViewDto {

    private String username;

    private String email;

    private String phoneNumber;

    private String photo;
}

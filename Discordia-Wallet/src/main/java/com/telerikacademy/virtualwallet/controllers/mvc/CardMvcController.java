package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.entities.Card;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.entities.dtos.CardDto;
import com.telerikacademy.virtualwallet.handlers.LoggerHandler;
import com.telerikacademy.virtualwallet.services.interfaces.CardService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import com.telerikacademy.virtualwallet.utils.CardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping("/cards")
public class CardMvcController {

    private final CardService cardService;
    private final CardMapper mapper;
    private final AuthenticationHelper helper;
    private final WalletService walletService;

    @Autowired
    public CardMvcController(CardService cardService,
                             CardMapper mapper,
                             AuthenticationHelper helper,
                             WalletService walletService) {
        this.cardService = cardService;
        this.mapper = mapper;
        this.helper = helper;
        this.walletService = walletService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute
    public Model getUserWallet(HttpSession session, Model model) {
        User user;
        try {
            user = helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return model;
        }
        return model.addAttribute("userWallet", walletService.getByUserId(user.getId()));
    }

    @GetMapping() // "/view"
    public String viewCardInfo(HttpSession session, Model model) {
        User user;
        try {
            user = helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("view-card", cardService.getByUserId(user.getId()));
        return "view-card";
    }

    @GetMapping("/add")
    public String showAddCardPage(HttpSession session, Model model) {
        User user;
        try {
            user = helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("addCard", new CardDto());
        return "card-add";
    }

    @PostMapping("/add")
    public String addCard(@Valid @ModelAttribute("view-card") CardDto cardDto,
                          HttpSession session,
                          BindingResult bindingResult,
                          Model model) {

        if (bindingResult.hasErrors()) {
            return "card-add";
        }

        User user;
        try {
            user = helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Card card = mapper.fromDto(cardDto,user);
            cardService.create(card);
            LoggerHandler.log("USER ADDED A CARD SUCCESSFULLY", Level.INFO, CardMvcController.class.getName());
            return "redirect:/";

        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("card_number","card_already_registered", e.getMessage());
            return "card-add";
        }
    }

    //todo: delete;

    @GetMapping("/{id}/delete")
    public String deleteCategory(@PathVariable int id, Model model,HttpSession session) {
        User user;
        try {
            user = helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }


        try {
            cardService.delete(user.getId());
            LoggerHandler.log("USER DELETED CARD SUCCESSFULLY", Level.INFO, this.getClass().getName());
            return "/transactions/successful-card-deletion";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "/errors/not-found";
        }catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/access-denied";
        }

    }


}

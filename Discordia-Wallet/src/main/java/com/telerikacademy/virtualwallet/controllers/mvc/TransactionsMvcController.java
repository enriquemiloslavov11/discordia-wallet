package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.InsufficientFundsException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.entities.*;
import com.telerikacademy.virtualwallet.entities.dtos.FilterTransactionDto;
import com.telerikacademy.virtualwallet.entities.dtos.TransactionDto;
import com.telerikacademy.virtualwallet.handlers.LoggerHandler;
import com.telerikacademy.virtualwallet.services.implementations.EmailSenderService;
import com.telerikacademy.virtualwallet.services.interfaces.*;
import com.telerikacademy.virtualwallet.utils.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Optional;
import java.util.logging.Level;

@Controller
@RequestMapping("/transactions")
public class TransactionsMvcController {

    private final String mailUsername = "noreply.virtualwallet@gmail.com";


    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final AuthenticationHelper helper;
    private final WalletService walletService;
    private final UserService userService;
    private final ConfirmationTokenService tokenService;
    private final EmailSenderService emailService;
    private final PendingTransactionService pendingTransactionService;
    private final CardService cardService;

    @Autowired
    public TransactionsMvcController(TransactionService service,
                                     TransactionMapper transactionMapper,
                                     AuthenticationHelper helper,
                                     WalletService walletService,
                                     UserService userService,
                                     ConfirmationTokenService tokenService,
                                     EmailSenderService emailService,
                                     PendingTransactionService pendingTransactionService,
                                     CardService cardService) {
        this.transactionService = service;
        this.transactionMapper = transactionMapper;
        this.helper = helper;
        this.walletService = walletService;
        this.userService = userService;
        this.tokenService = tokenService;
        this.emailService = emailService;
        this.pendingTransactionService = pendingTransactionService;
        this.cardService = cardService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (populateIsAuthenticated(session)){
            return session.getAttribute("role").toString().equalsIgnoreCase("admin");
        }
        return false;
    }

    @ModelAttribute
    public void getLoggedUser(HttpSession session, Model model) {
        if (populateIsAuthenticated(session)){
            User loggedUser = helper.tryGetUser(session);
            model.addAttribute("loggedUser" , userService.getById(loggedUser.getId(), loggedUser));
        }
    }


//    @GetMapping("/{id}")
//    public String showSingleTransaction(@PathVariable int id, Model model) {
//        try {
//            Transaction transaction = transactionService.getById(id);
//            model.addAttribute("transaction", transaction);
//            return "transaction";
//        } catch (EntityNotFoundException e) {
//            model.addAttribute("error", e.getMessage());
//            return "errors/not-found";
//        }
//    }

    @GetMapping
    public String showAllTransactions(HttpSession session, Model model, Pageable pageable) {
        User user;

        try {
            user = helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            model.addAttribute("allTransactions", transactionService.getAll(user));
            model.addAttribute("filterTransactionDto",new FilterTransactionDto());
            return "pagination-transactions";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "/errors/access-denied";
        }

    }

//    @GetMapping("/page")
//    public String showPageTransactions(HttpSession session) {
//        return "pagination-transactions";
//    }

    @GetMapping("/filter")
    public String filterTransaction(HttpSession session,@ModelAttribute("filterTransactionDto") FilterTransactionDto filterTransactionDto, Model model){

        try {
           User authenticated = helper.tryGetUser(session);
            Optional<LocalDate> fromDate=Optional.ofNullable(filterTransactionDto.getFromDate());
            Optional<LocalDate> toDate=Optional.ofNullable(filterTransactionDto.getToDate());
            Optional<String> sender=Optional.ofNullable(filterTransactionDto.getSender());
            Optional<String> recipient=Optional.ofNullable(filterTransactionDto.getRecipient());
            Optional<String> orderAmount=Optional.ofNullable(filterTransactionDto.getOrderAmount());
            Optional<String> orderDate = Optional.ofNullable(filterTransactionDto.getOrderDate());
            var filtered=transactionService.getAllFiltered(authenticated,fromDate,toDate,sender,recipient,orderAmount,orderDate);

            model.addAttribute("customerTransactions",filtered);
            model.addAttribute("filterTransactioDto",new FilterTransactionDto());
            return "customer-transactions";

       } catch (EntityNotFoundException e) {
            model.addAttribute("error",e.getMessage());
            return "errors/not-found";
        }catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}")
    public String showAllCustomerTransactions(@PathVariable int id, HttpSession session, Model model) {
        User user;

        try {
            user = helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (!UserHasCreditCard(user)) {
            return "redirect:/cards/add";
        }

        model.addAttribute("customerTransactions", transactionService.getAllUser(user, id));
        model.addAttribute("currentUser", user);
        return "customer-transactions";
    }

    private boolean UserHasCreditCard(User loggedUser) {
        try {
            cardService.getByUserId(loggedUser.getId());
            return true;
        }catch (EntityNotFoundException e) {
            return false;
        }
    }

    @GetMapping("/new")
    public String showCreateTransactionPage(Model model, HttpSession session) {

        if (!UserIsLogged(session)) {
            return "redirect:/auth/login";
        }

        model.addAttribute("transaction", new TransactionDto());
        return "transaction-new";
    }

    private boolean UserIsLogged(HttpSession session) {
        try {
            helper.tryGetUser(session);
            return true;
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @PostMapping("/new")
    public String createTransaction(@Valid @ModelAttribute ("transaction") TransactionDto transactionDto,
                                    BindingResult bindingResult,
                                    Model model,
                                    HttpServletRequest request,
                                    HttpSession session) {
        User moneySender;

        try {
            moneySender = helper.tryGetUser(session);
        }catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()){
            return "transaction-new";
        }

        User moneyRecipient;
        try {
            moneyRecipient = userService.getByField("username", transactionDto.getRecipient());
        } catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            bindingResult.rejectValue("recipient", "user with name %s not found", e.getMessage());
            return "transaction-new";
        }

        if (transactionNeedsVerification(transactionDto)){
            ConfirmationToken token = tokenService.createToken(moneySender);
            emailService.createTransactionVerificationMail(moneySender, token, request);
            LoggerHandler.log(String.format("EMAIL FOR TRANSACTION VERIFICATION WAS SUCCESSFULLY SENT TO: %s", moneySender.getEmail()), Level.INFO);

            createPendingTransaction(transactionDto, moneySender, moneyRecipient, token);
            model.addAttribute("emailId", moneySender.getEmail());
            LoggerHandler.log(String.format("USER WITH ID: %d CREATED A BIG TRANSACTION SUCCESSFULLY", moneySender.getId()), Level.INFO);

            return "transactions/transaction-confirmation";
        }

        try {
            Transaction transaction = transactionMapper.fromDto(transactionDto, moneySender);
            transactionService.create(transaction, moneySender);
            LoggerHandler.log(String.format("TRANSACTION WAS CREATED SUCCESSFULLY BY USER WITH ID: %d",moneySender.getId()), Level.INFO);

            return "/transactions/successful-transaction";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            bindingResult.rejectValue("username", "username not existent", e.getMessage());
            return "transaction-new";

        } catch (InsufficientFundsException e) {
            bindingResult.rejectValue("balance", "insufficient_funds", e.getMessage());
            return "transaction-new";

        } catch (UnauthorizedOperationException e){
            return "errors/blocked-user";
        }
    }

    private void createPendingTransaction(TransactionDto transactionDto, User moneySender, User moneyRecipient, ConfirmationToken token) {
        PendingTransaction pendingTransaction = transactionMapper.DtoToPending(transactionDto, moneySender, moneyRecipient, token);
        pendingTransactionService.create(pendingTransaction);
    }

    private boolean transactionNeedsVerification(TransactionDto transactionDto) {
        return transactionDto.getBalance().doubleValue() >= (1000);
    }


    @GetMapping("/confirm")
    public String confirmTransaction(Model model, @RequestParam("token") String confirmationToken) {

        ConfirmationToken token;
        User user;
        try {
            token = tokenService.findByConfirmationToken(confirmationToken);
            user = userService.getByField("id", String.valueOf(token.getUserId()));

        } catch (IllegalArgumentException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        }

        PendingTransaction pendingTransaction;
        try {
            pendingTransaction = pendingTransactionService.findTransactionByTokenId(token.getId());
        } catch (EntityNotFoundException e) {
            return "errors/not-found";
        }

        String transactionTokenValue = pendingTransaction.getToken().getConfirmationToken();

        if (transactionTokenValue.equals(token.getConfirmationToken())){

            if (token.getUserId() == pendingTransaction.getSender().getId()){

                Transaction confirmedTransaction = transactionMapper.pendingToObject(pendingTransaction);
                pendingTransactionService.delete(pendingTransaction);
                transactionService.create(confirmedTransaction, user);
                LoggerHandler.log(String.format("TRANSACTION OF USER WITH ID: %d WAS VERIFIED",token.getUserId()), Level.INFO);

                return "transactions/successful-transaction";
            }

        }

        return "errors/not-found";
    }
}

package com.telerikacademy.virtualwallet.controllers.rest;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.InsufficientFundsException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.entities.Transaction;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.entities.dtos.TransactionDto;
import com.telerikacademy.virtualwallet.entities.dtos.UserTransactionDto;
import com.telerikacademy.virtualwallet.services.interfaces.TransactionService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import com.telerikacademy.virtualwallet.utils.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/transactions")
public class TransactionController {
    private final static String BANK_URL = "http://localhost:8050/api/bank";

    private final static String UNAVAILABLE_FUNDS_ERROR_MSG = "Not enough funds in the card";

    private final AuthenticationHelper authenticationHelper;
    private final TransactionService transactionService;
    private final RestTemplate restTemplate;
    private final TransactionMapper transactionMapper;



    @Autowired
    public TransactionController(AuthenticationHelper authenticationHelper,
                                 TransactionService service,
                                 RestTemplate restTemplate,
                                 TransactionMapper transactionMapper) {
        this.authenticationHelper = authenticationHelper;
        this.transactionService = service;
        this.restTemplate = restTemplate;
        this.transactionMapper = transactionMapper;
    }

    @GetMapping
    public Page<Transaction> getPage(Pageable pageable) {
        return transactionService.transactionsPagination(pageable);
    }


    @GetMapping("/{id}")
    public List<UserTransactionDto> getAllForUser(@RequestHeader HttpHeaders headers,
                                                  @PathVariable int id,
                                                  @RequestParam (required = false) @DateTimeFormat(pattern ="yyyy-MM-dd") Optional<LocalDate> fromDate,
                                                  @RequestParam (required = false) @DateTimeFormat(pattern ="yyyy-MM-dd") Optional<LocalDate> toDate,
                                                  @RequestParam (required = false) Optional<String> sender,
                                                  @RequestParam (required = false) Optional<String> recipient,
                                                  @RequestParam (required = false) Optional<String> amount,
                                                  @RequestParam (required = false) Optional<String> date) {

        User user = authenticationHelper.tryGetUser(headers);
        return transactionService.getAllForUser(user,id,fromDate,toDate,sender,recipient,amount,date);

    }


    @PostMapping
    public Transaction createTransaction(@RequestHeader HttpHeaders headers, @Valid @RequestBody TransactionDto transactionDto) {

        User user = authenticationHelper.tryGetUser(headers);//sender
        Transaction transaction = transactionMapper.fromDto(transactionDto, user);
        transactionService.create(transaction,user);
        return transaction;
    }

    @PostMapping("/deposit")
    public Transaction deposit(@RequestHeader HttpHeaders headers, @Positive @RequestBody BigDecimal amount) {

        User loggedUser = authenticationHelper.tryGetUser(headers);//sender

        verifyMockBankResponse();

        Transaction transaction = transactionMapper.fromDto(amount, loggedUser);
        transactionService.create(transaction,loggedUser);
        return transaction;
    }

    private void verifyMockBankResponse() {
        HttpStatus bankResponse = restTemplate.getForObject(BANK_URL, HttpStatus.class);
        if(bankResponse != HttpStatus.OK){
            throw new InsufficientFundsException(UNAVAILABLE_FUNDS_ERROR_MSG);
        }
    }

    //    @GetMapping
//    public List<Transaction> getAll(@RequestHeader HttpHeaders headers) {
//
//         User user = authenticationHelper.tryGetUser(headers);
//         return transactionService.getAll(user);
//
//    }

}

package com.telerikacademy.virtualwallet.controllers;

import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.models.PasswordUtils;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";
    public static final String PASSWORD_EMPTY_MESSAGE = "Password can't be empty";


    private final UserService userService;

    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    //проверяваме дали User-a се е аутентикирал успешно
    public User tryGetUser(HttpHeaders headers) {

        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new AuthenticationFailureException("The requested resource requires authentication.");
        }

        String username = headers.getFirst(AUTHORIZATION_HEADER_NAME);
        return userService.getByField("username",username);
    }

    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");

        if (currentUser == null) {
            throw new AuthenticationFailureException("No user logged in.");
        }

        return userService.getByField("username",currentUser);
    }

    public User verifyAuthentication(String username, String password) {

        verifyPasswordIsNotEmpty(password);
        verifyUsernameIsNotEmpty(username);

        User user = verifyUsernameExists(username);
        verifyPasswordMatchesWithDB(user, password);

        return user;
    }

    private void verifyPasswordMatchesWithDB(User user, String password) {
        if (!PasswordUtils.verifyThePlainTextPassword(password, user.getHashedPassword(), user.getSalt())){
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }

    private User verifyUsernameExists(String username) {
        try {
            return userService.getByField("username", username);
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }

    private void verifyUsernameIsNotEmpty(String username) {
        if (username.isEmpty()){
            throw new AuthenticationFailureException(PASSWORD_EMPTY_MESSAGE);
        }
    }

    public void verifyPasswordIsNotEmpty(String password) {
        if (password.isEmpty()){
          throw new AuthenticationFailureException(PASSWORD_EMPTY_MESSAGE);
        }
    }
}

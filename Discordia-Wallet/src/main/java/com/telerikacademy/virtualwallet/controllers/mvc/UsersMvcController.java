package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.entities.dtos.SearchUserDto;
import com.telerikacademy.virtualwallet.entities.dtos.UserDto;
import com.telerikacademy.virtualwallet.entities.dtos.UserViewDto;
import com.telerikacademy.virtualwallet.entities.enums.Status;
import com.telerikacademy.virtualwallet.handlers.LoggerHandler;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import com.telerikacademy.virtualwallet.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;

@Controller
@RequestMapping("/users")
public class UsersMvcController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper helper;

    @Autowired
    public UsersMvcController(UserService userService,
                              AuthenticationHelper helper,
                              UserMapper mapper) {
        this.userService = userService;
        this.helper = helper;
        this.userMapper = mapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAuthorized")
    public boolean populateIsAuthorized(HttpSession session) {
        if (populateIsAuthenticated(session)){
            return session.getAttribute("role").toString().equalsIgnoreCase("admin");
        }
        return false;
    }

    @ModelAttribute
    public Model getLoggedUser(HttpSession session, Model model) {
        if (populateIsAuthenticated(session)){
            User loggedUser = helper.tryGetUser(session);
            return model.addAttribute("loggedUser" , userService.getById(loggedUser.getId(), loggedUser));
        }
        return model;
    }


    @GetMapping("/{id}")
    public String showUserAccount(@PathVariable int id,HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = helper.tryGetUser(session);
            User userToView = userService.getById(id, loggedUser);
            UserViewDto viewDto = userMapper.fromObjectToViewDto(userToView);
            model.addAttribute("user", viewDto);
            return "customer-account";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        }
    }
    @GetMapping("/{id}/status")
    public String showSingleUser (@PathVariable int id,HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = helper.tryGetUser(session);
            User userToView = userService.getById(id, loggedUser);
            model.addAttribute("userStatus", userToView);
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        }
    }
    @GetMapping
    public String showAllUsers(HttpSession session,Model model) {
        User user;
        try {
           user = helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("users", userService.getAll(user));
        model.addAttribute("keyword",new SearchUserDto());
        return "pagination-users";
    }

    @GetMapping("/{id}/update")
    public String showUpdateUserPage(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;

        try {
            loggedUser = helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user= userService.getById(id, loggedUser);
            model.addAttribute("updateUser", userMapper.toDto(user));

            return "customer-account-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());

            return "errors/not-found";
        }
    }

    @PostMapping("/{id}/status")
    public String setStatus(@PathVariable int id,
                            HttpSession session,
                            Model model) {
        try {
            User authenticated = helper.tryGetUser(session);
            User user = userMapper.setStatus(id);
            userService.setStatus(user,authenticated);
            model.addAttribute("userStatus",user);
            model.addAttribute("isBlocked",user.getStatus().equals(Status.BLOCK));

            return "user";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        }  catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/access-denied";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("updateUser")UserDto userDto,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {

        try {
            helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()){
            return "customer-account-update";
        }

        if (!userService.passwordMeetsRequirements(userDto.getPassword())){
            bindingResult.rejectValue("password", "password_error", "Password must contain at least 8 symbols, a capital letter, digit and special symbol");
            return "customer-account-update";
        }

        if (!userService.passwordMeetsRequirements(userDto.getConfirmPassword())){
            bindingResult.rejectValue("confirmPassword", "password_error", "Password must contain at least 8 symbols, a capital letter, digit and special symbol");
            return "customer-account-update";
        }

        if (!userDto.getPassword().equals(userDto.getConfirmPassword())){
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password Confirm should match password!");
            return "customer-account-update";
        }

        try {
            User userToUpdate = userMapper.fromDto(userDto,id);
            userService.update(userToUpdate);
            LoggerHandler.log(String.format("USER WITH ID: %d UPDATED HIS PROFILE SUCCESSFULLY", userToUpdate.getId()), Level.INFO);

            return "/transactions/user-updated-successfully";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/access-denied";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("phoneNumber", "duplicated_user_info", e.getMessage());
            return "customer-account-update";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(HttpSession session,@PathVariable int id, Model model) {
        try {
            User authenticated=helper.tryGetUser(session);
            userService.delete(id,authenticated);
            LoggerHandler.log(String.format("USER WITH ID: %d HAS BEEN DELETE SUCCESSFULLY", id), Level.INFO);

            return "redirect:/user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/access-denied";
        }
    }

    @PostMapping("/search")
    public String search(@ModelAttribute("keyword") SearchUserDto keyword, Model model)
    {
                model.addAttribute("users",userService.searchByKeyword(Optional.of(keyword.getKeyword())));
        return "users";
    }

    @GetMapping("/image")
    public String showUploadPage() {
        return "customer-image-update";
    }

    @PostMapping("/image")
    public String upload(MultipartFile file, HttpSession session) throws IOException {
        User user = helper.tryGetUser(session);
        userService.upload(file, user);
        return String.format("redirect:/users/%s",user.getId());
    }

}

package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.entities.ConfirmationToken;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.entities.Wallet;
import com.telerikacademy.virtualwallet.entities.dtos.RegisterDto;
import com.telerikacademy.virtualwallet.entities.dtos.UserViewDto;
import com.telerikacademy.virtualwallet.entities.dtos.WalletViewDto;
import com.telerikacademy.virtualwallet.handlers.LoggerHandler;
import com.telerikacademy.virtualwallet.services.implementations.EmailSenderService;
import com.telerikacademy.virtualwallet.services.interfaces.ConfirmationTokenService;
import com.telerikacademy.virtualwallet.services.interfaces.TransactionService;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import com.telerikacademy.virtualwallet.utils.UserMapper;
import com.telerikacademy.virtualwallet.utils.WalletMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.logging.Level;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final WalletService walletService;
    private final TransactionService transactionService;
    private final UserService userService;
    private final EmailSenderService emailSenderService;
    private final UserMapper userMapper;
    private final ConfirmationTokenService tokenService;
    private final WalletMapper walletMapper;

    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper,
                             WalletService walletService,
                             TransactionService transactionService,
                             UserService userService,
                             EmailSenderService emailSenderService,
                             UserMapper userMapper,
                             ConfirmationTokenService tokenService,
                             WalletMapper walletMapper) {

        this.authenticationHelper = authenticationHelper;
        this.walletService = walletService;
        this.transactionService = transactionService;
        this.userService = userService;
        this.emailSenderService = emailSenderService;
        this.userMapper = userMapper;
        this.tokenService = tokenService;
        this.walletMapper = walletMapper;
    }

    @GetMapping
    public String showHomePage(Model model) {
        model.addAttribute("registerDto", new RegisterDto());
        return "home-index";
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (populateIsAuthenticated(session)){
            return session.getAttribute("role").toString().equalsIgnoreCase("admin");
        }
        return false;
    }
    @ModelAttribute("isUser")
    public boolean populateIsUser(HttpSession session) {
        if (populateIsAuthenticated(session)){
            return session.getAttribute("role").toString().equalsIgnoreCase("user");
        }
        return false;
    }


    @ModelAttribute
    public Model getUserInfo(HttpSession session, Model model) {
        User user;
        Wallet wallet;
        try {
            user = authenticationHelper.tryGetUser(session);
            wallet = walletService.getByUserId(user.getId());
        } catch (AuthenticationFailureException | EntityNotFoundException e) {
            return model;
        }

        WalletViewDto walletViewDto = walletMapper.toViewDto(wallet);
        UserViewDto viewDto = userMapper.fromObjectToViewDto(user);

        model.addAttribute("user", viewDto);
        model.addAttribute("displayUserWallet", walletViewDto);
        return model.addAttribute("loggedUser" , userService.getById(user.getId(), user));
    }



    @PostMapping()
    public String handleHomeRegister(@Valid @ModelAttribute("registerDto") RegisterDto registerDto,
                                     BindingResult bindingResult,
                                     HttpSession session,
                                     HttpServletRequest request,
                                     Model model) {

        if (bindingResult.hasErrors()) {
            return "home-index";
        }

        if (!userService.passwordMeetsRequirements(registerDto.getPassword())) {
            bindingResult.rejectValue("password", "password_error", "Password must contain at least 8 symbols, a capital letter, digit and special symbol");
            return "home-index";
        }

        if (!registerDto.getPassword().equals(registerDto.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password Confirm should match password!");
        }

        User newUser = userMapper.fromDto(registerDto);
        try {
            userService.create(newUser);

        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "home-index";
        }

        ConfirmationToken token = tokenService.createToken(newUser);
        emailSenderService.createRegisterVerificationMail(newUser, token, request);
        model.addAttribute("emailId", newUser.getEmail());
        LoggerHandler.log("USER REGISTERED SUCCESSFULLY", Level.INFO, this.getClass().getName());

        return "/transactions/register-success";
    }

}

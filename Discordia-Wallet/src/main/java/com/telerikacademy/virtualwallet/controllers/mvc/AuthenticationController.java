package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.entities.ConfirmationToken;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.entities.dtos.LoginDto;
import com.telerikacademy.virtualwallet.entities.dtos.RegisterDto;
import com.telerikacademy.virtualwallet.entities.dtos.UserViewDto;
import com.telerikacademy.virtualwallet.handlers.LoggerHandler;
import com.telerikacademy.virtualwallet.services.interfaces.ConfirmationTokenService;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import com.telerikacademy.virtualwallet.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.logging.Level;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

//    @Value("spring.mail.username")
    public final static String SENDER_MAIL_USERNAME = "noreply.virtualwallet@gmail.com";

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ConfirmationTokenService tokenService;

    @Autowired
    public AuthenticationController(UserService userService,
                                    UserMapper userMapper,
                                    AuthenticationHelper authenticationHelper,
                                    WalletService walletService,
                                    ConfirmationTokenService tokenService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
        this.tokenService = tokenService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "register";
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "new-login";
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        LoggerHandler.log("USER HAS LOGGED OUT SUCCESSFULLY", Level.INFO);
        return "redirect:/";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              Model model,
                              BindingResult bindingResult,
                              HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "new-login";
        }

        try {
            User user = authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());

            if (!user.isVerified()){
                return "errors/not-verified";
            }

            session.setAttribute("currentUser", login.getUsername());
            session.setAttribute("role", user.getRole().getName());
            session.setAttribute("status", user.getStatus());

            UserViewDto viewDto = userMapper.fromObjectToViewDto(user);
            model.addAttribute("user", viewDto);

            LoggerHandler.log("USER LOGGED IN SUCCESSFULLY", Level.INFO, AuthenticationController.class.getName(), "handleLogin");
            return "redirect:/";

        } catch (AuthenticationFailureException | EntityNotFoundException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "new-login";
        }
    }

    @GetMapping("/verify")
    public String confirmUserAccount(Model model, @RequestParam("token") String confirmationToken) {

        ConfirmationToken token;
        try {
            token = tokenService.findByConfirmationToken(confirmationToken);
            System.out.println(token);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        }

        User user;
        try {
            user = userService.getByField("id", String.valueOf(token.getUserId()));
        } catch (EntityNotFoundException e) {
            return "redirect:/auth/login";
        }
        userMapper.verifyUser(user);
        userService.update(user);
        LoggerHandler.log("USER HAS REGISTERED SUCCESSFULLY", Level.INFO, AuthenticationController.class.getName(),"ConfirmUserAccount");

        return "transactions/account-verified";
    }
}
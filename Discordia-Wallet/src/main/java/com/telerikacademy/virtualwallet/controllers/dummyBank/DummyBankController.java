package com.telerikacademy.virtualwallet.controllers.dummyBank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/api/bank")
public class DummyBankController {
    private final Random random;

    @Autowired
    public DummyBankController(Random random) {
        this.random = random;
    }

    @GetMapping
    public HttpStatus isAccepted() {
        boolean isAccepted = random.nextBoolean();

        if (isAccepted) {
            return HttpStatus.OK;
        }
        return HttpStatus.BAD_REQUEST;
    }
}

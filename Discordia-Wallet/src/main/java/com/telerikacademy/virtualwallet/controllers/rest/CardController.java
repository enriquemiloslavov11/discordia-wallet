package com.telerikacademy.virtualwallet.controllers.rest;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.entities.Card;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.entities.dtos.CardDto;
import com.telerikacademy.virtualwallet.services.interfaces.CardService;
import com.telerikacademy.virtualwallet.utils.CardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/cards")
public class CardController {
    private final CardService cardService;
    private final CardMapper cardMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CardController(CardService cardService, CardMapper cardMapper, AuthenticationHelper authenticationHelper) {
        this.cardService = cardService;
        this.cardMapper = cardMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping()
    public Card add( @RequestHeader HttpHeaders headers,
                      @Valid @RequestBody CardDto cardDto) {

        User user = authenticationHelper.tryGetUser(headers);
        Card card = cardMapper.fromDto(cardDto,user);
        cardService.create(card);
        return card;
    }

    @GetMapping()
    public Card view(
            @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        return cardService.getByUserId(user.getId());
    }

    @DeleteMapping()
    public void delete(@RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        cardService.delete(user.getId());
    }
}

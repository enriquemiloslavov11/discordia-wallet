package com.telerikacademy.virtualwallet.repositories.interfaces;

import com.telerikacademy.virtualwallet.entities.Role;

import java.util.List;

public interface RoleRepository {
    List<Role> getAll();

    Role getById(int id);
}

package com.telerikacademy.virtualwallet.repositories.interfaces;

import com.telerikacademy.virtualwallet.entities.Transaction;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TransactionRepository {

    Transaction getById(int id);

    Transaction getByField(String field, String value);

    void create(Transaction transaction);

    List<Transaction> getAll();

//    List<Transaction> getAllForUser(int userId, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<Integer> senderId, Optional<Integer> recipientId, Optional<String> amount, Optional<String> date);

    List<Transaction> getAllUser(int userId);

    List<Transaction> getAllFiltered(int userId, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<Integer> senderId, Optional<Integer> recipientId, Optional<String> amount, Optional<String> date);

}

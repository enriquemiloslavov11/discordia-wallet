package com.telerikacademy.virtualwallet.repositories.implementations;

import com.telerikacademy.virtualwallet.entities.PendingTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PendingTransactionsRepository extends JpaRepository<PendingTransaction, Long> {

    PendingTransaction findTransactionById(int transactionId);

    PendingTransaction findTransactionBySenderId(int id);

    PendingTransaction findTransactionByRecipientId(int id);

    PendingTransaction findPendingTransactionByTokenId(int tokenId);
}

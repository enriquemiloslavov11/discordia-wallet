package com.telerikacademy.virtualwallet.repositories.interfaces;

import com.telerikacademy.virtualwallet.entities.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> getAll();

    User getById(int id);

    void create(User user);

    void update(User user);

    void delete(int id);

    User getByField(String field,String value);

    User getByAllFields(String value);

    List<User> searchByKeyword (Optional<String> word);

}

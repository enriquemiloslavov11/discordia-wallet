package com.telerikacademy.virtualwallet.repositories.implementations;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.entities.Card;
import com.telerikacademy.virtualwallet.repositories.interfaces.CardRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CardRepositoryImpl implements CardRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CardRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Card> getAll() {
        return null;
    }

    @Override
    public Card getByUserId(int id) {
        try (Session session = sessionFactory.openSession()) {

            Query<Card> query = session.createQuery(" from Card card WHERE card.user.id = :id", Card.class);
            query.setParameter("id",id);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Card");
            }

            return query.list().get(0);
        }
    }

    @Override
    public Card getByField(String field,String value) {
        try (Session session = sessionFactory.openSession()) {
            Query<Card> query = session.createQuery(String.format("from Card where %s = '%s'", field, value), Card.class);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Card", field, value);
            }

            return query.list().get(0);
        }

    }

    @Override
    public void create(Card card) {
            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.save(card);
                session.getTransaction().commit();
            }
        }


    @Override
    public void update(Card card) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(card);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int userId) {
            Card cardToDelete = getByUserId(userId);
            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.delete(cardToDelete);
                session.getTransaction().commit();
            }
        }
    }


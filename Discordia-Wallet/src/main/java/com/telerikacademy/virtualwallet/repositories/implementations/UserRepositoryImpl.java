package com.telerikacademy.virtualwallet.repositories.implementations;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.repositories.interfaces.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    public User getByAllFields(String value) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where (username = :value) OR (phoneNumber = :value) OR (email = :value)", User.class);
            query.setParameter("value", value);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "username, email or phone number:", value);
            }

            return query.list().get(0);
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public User getByField(String field,String value) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(String.format("from User where %s = '%s'", field, value), User.class);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("User", field, value);
            }

            return query.list().get(0);
        }

    }

    @Override
    public List<User> searchByKeyword(Optional<String> word) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from User where (:word is null or username like concat('%',:word,'%')) or " +
                    "(:word is null or phone_number like concat('%',:word,'%')) or (:word is null or email like concat('%',:word,'%'))";
            Query<User> query = session.createQuery(queryString, User.class);
            query.setParameter("word", word.orElse(null));

            return query.list();
        }
    }
}

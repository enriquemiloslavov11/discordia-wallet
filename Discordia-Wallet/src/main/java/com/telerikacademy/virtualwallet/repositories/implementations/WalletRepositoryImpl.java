package com.telerikacademy.virtualwallet.repositories.implementations;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.entities.Wallet;
import com.telerikacademy.virtualwallet.repositories.interfaces.WalletRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WalletRepositoryImpl implements WalletRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public WalletRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void update(Wallet wallet) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(wallet);
            session.getTransaction().commit();
        }
    }

    @Override
    public void create(Wallet wallet) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(wallet);
            session.getTransaction().commit();
        }
    }

    @Override
    public Wallet getByUserId(int id) {
        try (Session session = sessionFactory.openSession()) {

            Query<Wallet> query = session.createQuery(" from Wallet wallet WHERE wallet.user.id = :id", Wallet.class);
            query.setParameter("id",id);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Wallet");
            }

            return query.list().get(0);
        }
    }

    @Override
    public Wallet getById(int id) {
        try (Session session = sessionFactory.openSession()) {
           Wallet wallet = session.get(Wallet.class, id);
            if (wallet == null) {
                throw new EntityNotFoundException("Wallet", id);
            }
            return wallet;
        }
    }
}

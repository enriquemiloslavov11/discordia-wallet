package com.telerikacademy.virtualwallet.repositories.interfaces;

import com.telerikacademy.virtualwallet.entities.Wallet;

public interface WalletRepository {


    void update(Wallet wallet);

    void create(Wallet wallet);

    Wallet getByUserId(int userId);

    Wallet getById(int walletId);
}

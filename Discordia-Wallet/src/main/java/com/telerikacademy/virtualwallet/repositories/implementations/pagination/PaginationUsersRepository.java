package com.telerikacademy.virtualwallet.repositories.implementations.pagination;

import com.telerikacademy.virtualwallet.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaginationUsersRepository extends JpaRepository<User, Long> {

    @Override
    Page<User> findAll( Pageable pageable);


}

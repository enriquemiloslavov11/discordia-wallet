package com.telerikacademy.virtualwallet.utils;

import com.telerikacademy.virtualwallet.entities.Card;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.entities.dtos.CardDto;
import com.telerikacademy.virtualwallet.entities.enums.CardType;
import com.telerikacademy.virtualwallet.repositories.interfaces.CardRepository;
import com.telerikacademy.virtualwallet.repositories.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CardMapper {
    private final UserRepository userRepository;
    private final CardRepository cardRepository;

    @Autowired
    public CardMapper(UserRepository userRepository, CardRepository cardRepository) {
        this.userRepository = userRepository;
        this.cardRepository = cardRepository;
    }
    public Card fromDto(CardDto cardDto,User user) {
        Card card= new Card();
        dtoToObject(cardDto, card,user);
        return card;
    }

   /* public Card fromDto(CardDto cardDto, int userId) {
        Card card = cardRepository.getByUserId(userId);
        dtoToObject(cardDto, card);
        return card;
    }*/

    private void dtoToObject(CardDto cardDto, Card card,User user) {
        card.setCardNumber(cardDto.getCardNumber());
        card.setExpirationDate(cardDto.getExpirationDate());
        card.setCardHolder(cardDto.getCardHolder());
        card.setCheckNumber(cardDto.getCheckNumber());
        card.setCardType(CardType.CREDIT);
        card.setUser(user);
    }
}

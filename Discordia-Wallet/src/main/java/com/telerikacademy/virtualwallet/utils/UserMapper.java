package com.telerikacademy.virtualwallet.utils;

import com.telerikacademy.virtualwallet.entities.Role;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.entities.Wallet;
import com.telerikacademy.virtualwallet.entities.dtos.RegisterDto;
import com.telerikacademy.virtualwallet.entities.dtos.UserDto;
import com.telerikacademy.virtualwallet.entities.dtos.UserViewDto;
import com.telerikacademy.virtualwallet.entities.enums.Status;
import com.telerikacademy.virtualwallet.handlers.LoggerHandler;
import com.telerikacademy.virtualwallet.models.PasswordUtils;
import com.telerikacademy.virtualwallet.repositories.interfaces.RoleRepository;
import com.telerikacademy.virtualwallet.repositories.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.logging.Level;

@Component
public class UserMapper {
    private static final Status UNBLOCK = Status.UNBLOCK;
    private static final Status BLOCK = Status.BLOCK;

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserMapper(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }
    public UserViewDto fromObjectToViewDto(User user) {
        UserViewDto viewDto = new UserViewDto();
        viewDto.setUsername(user.getUsername());
        viewDto.setEmail(user.getEmail());
        viewDto.setPhoneNumber(user.getPhoneNumber());
        viewDto.setPhoto(user.getUserPhoto());
        return viewDto;
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        objectToDto(user, userDto);
        return userDto;
    }

    public User fromDto(UserDto userDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(userDto, user);
        return user;
    }



    public User setStatus(int id) {
        User user = userRepository.getById(id);
        if (user.getStatus() != BLOCK) {
            user.setStatus(Status.values()[user.getStatus().ordinal() + 1]);
            LoggerHandler.log(String.format("USER WITH ID: %d HAS BEEN BLOCKED", user.getId()), Level.INFO);
        } else {
            user.setStatus(UNBLOCK);
            LoggerHandler.log(String.format("USER WITH ID: %d HAS BEEN UNBLOCKED", user.getId()), Level.INFO);
        }
        return user;
    }

    public void verifyUser(User user) {
        user.setVerified(true);
    }

    public User fromDto(RegisterDto registerDto) {
        User user = new User();
        user.setUsername(registerDto.getUsername());
        user.setEmail(registerDto.getEmail().toLowerCase());
        user.setPhoneNumber(registerDto.getPhoneNumber());
        createUserPassword(registerDto.getPassword(), user);

        Role role = new Role();
        role.setId(2);
        role.setName("User");
        user.setRole(role);

        user.setStatus(Status.UNBLOCK);
        user.setUserPhoto("png2");
        user.setVerified(false);

        Wallet wallet=new Wallet();

        wallet.setUser(user);
        wallet.setBalance(BigDecimal.ZERO);

        return user;
    }

    private void createUserPassword(String password, User user) {
        String salt = PasswordUtils.generateSalt(10).get();
        String hashedPassword = PasswordUtils.hashThePlainTextPassword(password, salt).get();

        user.setHashedPassword(hashedPassword);
        user.setSalt(salt);
    }

    private void objectToDto(User user, UserDto userDto) {
        userDto.setUserPhoto(user.getUserPhoto());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getHashedPassword());
        userDto.setConfirmPassword(user.getHashedPassword());
        userDto.setEmail(user.getEmail());
        userDto.setPhoneNumber(user.getPhoneNumber());
        userDto.setStatus(user.getStatus());
    }

    private void dtoToObject(UserDto userDto, User user) {
        user.setUsername(userDto.getUsername());
        createUserPassword(userDto.getPassword(), user);
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setStatus(Status.UNBLOCK);
    }

}

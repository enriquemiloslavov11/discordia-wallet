package com.telerikacademy.virtualwallet.services.implementations;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.entities.ConfirmationToken;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.repositories.interfaces.ConfirmationTokenRepository;
import com.telerikacademy.virtualwallet.services.interfaces.ConfirmationTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {

    private final ConfirmationTokenRepository tokenRepository;

    @Override
    public void createToken(ConfirmationToken confirmationToken) {
        tokenRepository.save(confirmationToken);
    }

    @Override
    public ConfirmationToken findByConfirmationToken(String confirmationToken) {
        if (!tokenRepository.existsByConfirmationToken(confirmationToken)) {
            throw new EntityNotFoundException("Entity Not Found !");
        }
        return tokenRepository.findByConfirmationToken(confirmationToken);
    }

    @Override
    public ConfirmationToken createToken(User user) {
        ConfirmationToken token = new ConfirmationToken(user.getId());
        createToken(token);
        return token;
    }
}
package com.telerikacademy.virtualwallet.services.interfaces;

import com.telerikacademy.virtualwallet.entities.Transaction;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.entities.Wallet;

public interface WalletService {

   void create(User user);

   void update(Transaction transaction);

    void update(Wallet wallet);

    Wallet getByUserId(int userId);

    Wallet getById(int walletId);

}

package com.telerikacademy.virtualwallet.services.implementations;

import com.telerikacademy.virtualwallet.controllers.CloudinaryHelper;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.repositories.implementations.pagination.PaginationUsersRepository;
import com.telerikacademy.virtualwallet.repositories.interfaces.UserRepository;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private static final String DELETE_USER_ERROR_MESSAGE = "Access denied";
    private static final String MODIFY_USER_ERROR_MESSAGE = "Only admin can modify user status.";
    private static final String GET_USERS_ERROR_MESSAGE = "Only admin can view details of all users." ;
    private static final String GET_USER_ERROR_MESSAGE = "Only admin can view details of user.";

    private final UserRepository userRepository;
    private final WalletService walletService;
    private final CloudinaryHelper cloudinaryHelper;
    private final PaginationUsersRepository paginationUsersRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           WalletService walletService,
                           CloudinaryHelper cloudinaryHelper,
                           PaginationUsersRepository paginationUsersRepository) {
        this.userRepository = userRepository;
        this.walletService = walletService;
        this.cloudinaryHelper = cloudinaryHelper;
        this.paginationUsersRepository = paginationUsersRepository;
    }

    @Override
    public Page<User> userPagination(Pageable pageable) {
        return paginationUsersRepository.findAll(pageable);
    }

    @Override
    public List<User> getAll(User user)  {
        if (!user.isUser("admin")) {
            throw new UnauthorizedOperationException(GET_USERS_ERROR_MESSAGE);
        }
        return userRepository.getAll();
    }


    @Override
    public List<User> searchByKeyword(Optional<String> word) {
        return userRepository.searchByKeyword(word);
    }

    @Override
    public User getById(int userToGetId, User loggedUser) {

        checkIfUserIsAllowed(userToGetId, loggedUser);
        return userRepository.getById(userToGetId);


    }

    @Override
    public User getByField(String field,String value) {
        return userRepository.getByField(field,value);
    }

    @Override
    public void create(User user) {
        createCheckIfUsernameExists(user);
        createCheckIfEmailExists(user);
        createCheckIfPhoneNumberExists(user);

        userRepository.create(user);
        walletService.create(user);
    }

    @Override
    public void update(User user) {
        updateCheckIfEmailExists(user);
        updateCheckIfPhoneNumberExists(user);
        userRepository.update(user);
    }

    @Override
    public void delete(int id, User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(DELETE_USER_ERROR_MESSAGE);
        }
        userRepository.delete(id);
    }

    @Override
    public void upload(MultipartFile file, User user) throws IOException {
        String url = cloudinaryHelper.upload(file);
        user.setUserPhoto(url);
        userRepository.update(user);
    }

    private void createCheckIfPhoneNumberExists(User user) {
        //check if phone number is already in use

        boolean duplicateExists = true;

        try{
            userRepository.getByField("phone_number", user.getPhoneNumber());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "phone number", user.getPhoneNumber());
        }
    }

    private void createCheckIfEmailExists(User user) {
        //check if email is already in use

         boolean duplicateExists = true;

        try {
            userRepository.getByField("email", user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists){
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
    }

    private void createCheckIfUsernameExists(User user) {
        //check if username Is Already In Use

        boolean duplicateExists = true;

        try {
            userRepository.getByField("username",user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
    }

    private void updateCheckIfPhoneNumberExists(User user) {
        boolean duplicateExists = true;

        try {
            User existingUser = userRepository.getByField("phone_number", user.getPhoneNumber());

            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }

        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "phone number", user.getPhoneNumber());
        }
    }

    private void updateCheckIfEmailExists(User user) {
        boolean duplicateExists = true;
        try {
            User existingUser = userRepository.getByField("email", user.getEmail());

            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }

        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
    }

    private void checkIfUserIsAllowed(int userToGetId, User loggedUser) {
        if (loggedUser.getId()!=userToGetId && loggedUser.isUser("user")) {
            throw new UnauthorizedOperationException(GET_USER_ERROR_MESSAGE);
        }
    }

    @Override
    public void setStatus(User user, User loggedUser) {
        if (!loggedUser.isAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }
        userRepository.update(user);
    }

    //    @Override
//    public String getUserStatus(int id, User user) {
//        if (user.getId()!=id && user.isUser("user")) {
//            throw new UnauthorizedOperationException(GET_USER_ERROR_MESSAGE);
//        }
//        return repository.getUserStatus(id);
//    }


    public boolean passwordMeetsRequirements(String password) {

        //checks if password contains
        // at least 1 specialCharacter
        // at least 1 upperCase letter
        // at least 1 lowerCase letter
        // has at least 8 characters

        if (password.length() < 8) {
            return false;
        }

        boolean containsDigit = false;
        boolean containsCapitalLetter = false;
        boolean containsSpecialCharacter = false;

        char[] charArr = password.toCharArray();

        for (char current: charArr) {

            //ascii values for special characters
            if (!containsSpecialCharacter) {

                if (current >= 33 && current <= 47
                        || current >= 58 && current <= 64
                        || current >= 91 && current <= 96
                        || current >= 123 && current <= 126) {

                    containsSpecialCharacter = true;
                }
            }

            if (!containsCapitalLetter){

                if (Character.isLetter(current) && Character.isUpperCase(current)) {
                    containsCapitalLetter = true;
                }
            }

            if (!containsDigit) {

                if (Character.isDigit(current)){
                    containsDigit = true;
                }
            }


            if (containsSpecialCharacter && containsCapitalLetter && containsDigit) {
                return true;
            }
        }
        return false;
    }

}

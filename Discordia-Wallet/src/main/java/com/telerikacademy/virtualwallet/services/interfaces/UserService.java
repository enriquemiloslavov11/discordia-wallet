package com.telerikacademy.virtualwallet.services.interfaces;


import com.telerikacademy.virtualwallet.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAll(User user);

    User getById(int id, User user);

    void create(User user);

    void update(User user);

    void setStatus(User user,User authenticated);

    void delete(int id, User user);

    User getByField(String field,String value);
    //String getUserStatus(int id, User user);

    List<User> searchByKeyword (Optional<String> word);

    Page<User> userPagination(Pageable pageable);

    boolean passwordMeetsRequirements(String password);

    void upload(MultipartFile file, User user)throws IOException;
}

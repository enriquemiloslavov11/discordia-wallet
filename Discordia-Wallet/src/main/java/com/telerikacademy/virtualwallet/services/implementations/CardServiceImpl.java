package com.telerikacademy.virtualwallet.services.implementations;

import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.entities.Card;
import com.telerikacademy.virtualwallet.exceptions.ImpossibleOperationException;
import com.telerikacademy.virtualwallet.repositories.interfaces.CardRepository;
import com.telerikacademy.virtualwallet.services.interfaces.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardServiceImpl implements CardService {
    private static final String USER_ALREADY_HAS_CARD = "Y";
    private final CardRepository repository;

    @Autowired
    public CardServiceImpl(CardRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Card> getAll() {
        return repository.getAll();
    }

    @Override
    public Card getByUserId(int id) {
        return repository.getByUserId(id);
    }

    @Override
    public Card getByField(String field,String value) {
        return repository.getByField(field,value);
    }

    @Override
    public void create(Card card) {
        verifyCardIsNotDuplicate(card);
        verifyUserDoesNotHaveExistingCard(card.getUser().getId());
        repository.create(card);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }

    private void verifyCardIsNotDuplicate(Card card) {
        boolean duplicateExists = true;
        try {
            repository.getByField("card_number",card.getCardNumber());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Card", "card number", card.getCardNumber());
        }
    }

    private void verifyUserDoesNotHaveExistingCard(int userId) {
        boolean userHasCard = true;
        try {
            getByUserId(userId);
        } catch (EntityNotFoundException e) {
            userHasCard = false;
        }

        if (userHasCard) {
            throw new ImpossibleOperationException("User",userId,"CreditCard");
        }
    }
}

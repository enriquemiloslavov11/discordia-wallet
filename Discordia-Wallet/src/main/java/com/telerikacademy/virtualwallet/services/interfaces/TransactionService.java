package com.telerikacademy.virtualwallet.services.interfaces;

import com.telerikacademy.virtualwallet.entities.Transaction;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.entities.dtos.UserTransactionDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TransactionService {

    Transaction getById(int id);

    Transaction getByField(String field, String value);

    void create(Transaction transaction, User user);

    List<Transaction> getAll(User user);

    Page<Transaction> transactionsPagination(Pageable pageable);

    List<UserTransactionDto> getAllForUser(User authenticated, int id, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<String> sender, Optional<String> recipient, Optional<String> amount, Optional<String> date);

    List<UserTransactionDto> getAllUser(User authenticated, int id);

    List<UserTransactionDto> getAllFiltered(User authenticated, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<String> sender, Optional<String> recipient, Optional<String> amount, Optional<String> date);

}

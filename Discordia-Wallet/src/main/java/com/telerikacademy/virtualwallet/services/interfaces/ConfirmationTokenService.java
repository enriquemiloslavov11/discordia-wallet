package com.telerikacademy.virtualwallet.services.interfaces;

import com.telerikacademy.virtualwallet.entities.ConfirmationToken;
import com.telerikacademy.virtualwallet.entities.User;

public interface ConfirmationTokenService {

    void createToken(ConfirmationToken confirmationToken);

    ConfirmationToken findByConfirmationToken(String confirmationToken);

    ConfirmationToken createToken(User user);

}
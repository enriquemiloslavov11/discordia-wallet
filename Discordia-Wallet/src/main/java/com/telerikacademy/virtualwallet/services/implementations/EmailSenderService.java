package com.telerikacademy.virtualwallet.services.implementations;

import com.telerikacademy.virtualwallet.controllers.mvc.AuthenticationController;
import com.telerikacademy.virtualwallet.entities.ConfirmationToken;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.services.interfaces.ConfirmationTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
public class EmailSenderService {

    private static final String MAIL_USERNAME = "noreply.virtualwallet@gmail.com";

    private final JavaMailSender javaMailSender;
    private ConfirmationTokenService tokenService;

    @Autowired
    public EmailSenderService(JavaMailSender javaMailSender, ConfirmationTokenService tokenService) {
        this.javaMailSender = javaMailSender;
        this.tokenService = tokenService;
    }

    @Async
    public void sendEmail(SimpleMailMessage email) {
        javaMailSender.send(email);
    }

    public void createTransactionVerificationMail(User moneySender, ConfirmationToken token, HttpServletRequest request) {

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(moneySender.getEmail());
        mailMessage.setSubject("Verify transaction!");
        mailMessage.setFrom(MAIL_USERNAME);
        mailMessage.setText("To confirm your Transaction, please click the following link : "
                + getUrl(request,token, "/transactions") + "/confirm?token=" + token.getConfirmationToken());

        sendEmail(mailMessage);
    }

    public void createRegisterVerificationMail(User newUser, ConfirmationToken token, HttpServletRequest request){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(newUser.getEmail());
        mailMessage.setSubject("Complete Registration!");
        mailMessage.setFrom(AuthenticationController.SENDER_MAIL_USERNAME);
        mailMessage.setText("To confirm your account, please click here :  "
                + getUrl(request, token, "auth") + "/verify?token=" + token.getConfirmationToken());

        sendEmail(mailMessage);
    }

    private String getUrl(HttpServletRequest request, ConfirmationToken token, String urlReplacement) {

        String siteUrl = request.getRequestURL().toString();
        siteUrl = siteUrl + urlReplacement;
        return siteUrl;
    }

}

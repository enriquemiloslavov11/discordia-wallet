package com.telerikacademy.virtualwallet.services.interfaces;

import com.telerikacademy.virtualwallet.entities.PendingTransaction;

public interface PendingTransactionService {

    void create(PendingTransaction transaction);

    void delete(PendingTransaction transaction);

    PendingTransaction findTransactionById(int TransactionId);

    PendingTransaction findTransactionBySenderId(int senderId);

    PendingTransaction findTransactionByRecipientId(int recipientId);

    PendingTransaction findTransactionByTokenId(int tokenId);

}

package com.telerikacademy.virtualwallet.services.interfaces;

import com.telerikacademy.virtualwallet.entities.Role;

import java.util.List;

public interface RoleService {
    List<Role> getAll();

    Role getById(int id);
}

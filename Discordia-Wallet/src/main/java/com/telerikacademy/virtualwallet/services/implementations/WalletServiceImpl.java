package com.telerikacademy.virtualwallet.services.implementations;

import com.telerikacademy.virtualwallet.exceptions.InsufficientFundsException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.entities.Transaction;
import com.telerikacademy.virtualwallet.entities.User;
import com.telerikacademy.virtualwallet.entities.Wallet;
import com.telerikacademy.virtualwallet.entities.enums.Status;
import com.telerikacademy.virtualwallet.repositories.interfaces.WalletRepository;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class WalletServiceImpl implements WalletService {

    private static final String USER_BLOCKED_ERROR_MESSAGE = "Blocked user is not allowed to create transactions.";
    private final WalletRepository walletRepository;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    @Override
    public Wallet getByUserId(int userId) {
        return walletRepository.getByUserId(userId);
    }

    @Override
    public Wallet getById(int walletId) {
        return walletRepository.getById(walletId);
    }

    @Override
    public void create(User user) {
        Wallet wallet= new Wallet();
        wallet.setUser(user);
        wallet.setBalance(BigDecimal.ZERO);
        walletRepository.create(wallet);
    }

    @Override
    public void update(Transaction transaction) {

        int senderId = transaction.getSender().getId();
        int recipientId = transaction.getRecipient().getId();

        Wallet senderWallet;
        Wallet recipientWallet;

       if (senderId != recipientId) {

           senderWallet = getByUserId(senderId);

           verifySenderIsNotBlocked(transaction);
           verifySenderHasEnoughFunds(senderWallet, transaction);

           senderWallet.setBalance(senderWallet.getBalance().subtract(transaction.getAmount()));
           walletRepository.update(senderWallet);
       }

       recipientWallet = getByUserId(recipientId);
       recipientWallet.setBalance(recipientWallet.getBalance().add(transaction.getAmount()));
       walletRepository.update(recipientWallet);
    }

    private void verifySenderHasEnoughFunds(Wallet senderWallet, Transaction transaction) {
        if (senderWallet.getBalance().compareTo(transaction.getAmount()) < 0) {
            throw new InsufficientFundsException("not enough funds");
        }
    }

    private void verifySenderIsNotBlocked(Transaction transaction) {
        if(transaction.getSender().getStatus().equals(Status.valueOf("BLOCK"))){
            throw new UnauthorizedOperationException(USER_BLOCKED_ERROR_MESSAGE);
        }
    }

    @Override
    public void update(Wallet wallet) {
        walletRepository.update(wallet);
    }


}

package com.telerikacademy.virtualwallet.services.implementations;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.entities.PendingTransaction;
import com.telerikacademy.virtualwallet.repositories.implementations.PendingTransactionsRepository;
import com.telerikacademy.virtualwallet.services.interfaces.PendingTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PendingTransactionServiceImpl implements PendingTransactionService {

    private static final String TRANSACTION_NOT_FOUND = "Transaction not found";
    private final PendingTransactionsRepository pendingRepository;

    @Autowired
    public PendingTransactionServiceImpl(PendingTransactionsRepository pendingRepository) {
        this.pendingRepository = pendingRepository;
    }

    @Override
    public void create(PendingTransaction transaction) {
        pendingRepository.save(transaction);
    }

    @Override
    public void delete(PendingTransaction transaction) {
        pendingRepository.delete(transaction);
    }

    @Override
    public PendingTransaction findTransactionById(int transactionId) {
        PendingTransaction pendingTransaction = pendingRepository.findTransactionById(transactionId);
        if (pendingTransaction == null) {
            throw new EntityNotFoundException(TRANSACTION_NOT_FOUND);
        }
        return pendingTransaction;
    }

    @Override
    public PendingTransaction findTransactionBySenderId(int senderId) {
        PendingTransaction pendingTransaction =  pendingRepository.findTransactionBySenderId(senderId);
        if (pendingTransaction == null) {
            throw new EntityNotFoundException(TRANSACTION_NOT_FOUND);
        }
        return pendingTransaction;
    }

    @Override
    public PendingTransaction findTransactionByRecipientId(int recipientId) {
       PendingTransaction pendingTransaction = pendingRepository.findTransactionByRecipientId(recipientId);
        if (pendingTransaction == null) {
            throw new EntityNotFoundException(TRANSACTION_NOT_FOUND);
        }
        return pendingTransaction;
    }

    @Override
    public PendingTransaction findTransactionByTokenId(int tokenId) {
        PendingTransaction pendingTransaction = pendingRepository.findPendingTransactionByTokenId(tokenId);
        if (pendingTransaction  == null){
            throw new EntityNotFoundException(TRANSACTION_NOT_FOUND);
        }
        return pendingTransaction;
    }
}

package com.telerikacademy.virtualwallet.services.interfaces;

import com.telerikacademy.virtualwallet.entities.Card;

import java.util.List;

public interface CardService {

    List<Card> getAll();

    Card getByUserId(int id);

    Card getByField(String field,String value);

    void create(Card card);

    void delete(int id);
}

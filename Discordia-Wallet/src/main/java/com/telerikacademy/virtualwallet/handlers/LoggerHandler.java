package com.telerikacademy.virtualwallet.handlers;



import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggerHandler {

    private static Logger logger;
    private static FileHandler fh;

    static {
        logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        try {
            fh = new FileHandler("C:/temp/test/MyLogFile.log");
            setLoggerConfiguration();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void setLoggerConfiguration() {
        try {
            logger.addHandler(fh);
            logger.setUseParentHandlers(false);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);


        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public static void log(String message, Level level) {
        checkLevelAndLogMessage(level, message);
    }

    public static void log(String message , Level level, String className) {
        message = buildMessage(message, className);
        checkLevelAndLogMessage(level, message);
    }

    public static void log(String message, Level level, String className, String methodName) {
       message = buildMessage(message,className, methodName);
        checkLevelAndLogMessage(level, message);
    }

    private static String buildMessage(String message, String className) {
        return String.format("%n CLASS: %s%n %s",className, message);
    }

    private static String buildMessage(String message, String className, String methodName) {
        return String.format("%n CLASS: %s%n METHOD: %s%n %s",className, methodName, message);
    }

    private static void checkLevelAndLogMessage(Level level, String message) {

        if (level.equals(Level.INFO)) {
            logger.info(message);
        }
        else if (level.equals(Level.WARNING)) {
            logger.warning(message);
        }
        else if (level.equals(Level.FINE)) {
            logger.fine(message);
        }
         else if (level.equals(Level.FINER)) {
            logger.finer(message);
        }
        else if (level.equals(Level.FINEST)) {
            logger.finest(message);
        }
        else if (level.equals(Level.SEVERE)) {
            logger.severe(message);
        }
        else {
            logger.info(message);
        }
    }

}

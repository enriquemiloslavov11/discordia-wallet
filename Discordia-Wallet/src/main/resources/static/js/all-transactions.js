"use strict"
$ (function () {
    getAndLoadContent(12, 0);
    nextPage();
    previousPage();
});

function previousPage() {
    $("#previous-page").on("click", function () {
        getAndLoadContent(12, parseInt($("#current-page-value").text()) -2);
    })
}

function nextPage() {
    $("#next-page").on("click", function () {
        getAndLoadContent(12, parseInt($("#current-page-value").text()));
    })
}

function enableDisableButton($button, shouldDisabled) {
    if (shouldDisabled) {
        $button.addClass("li-disabled");
    } else if ($button.hasClass("li-disabled")) {
        $button.removeClass("li-disabled");
    }
}

function getAndLoadContent(size, page) {
    $.ajax({
        url: "/api/transactions",
        type: "GET",
        data: {
            size: size,
            page: page,
        },

        success: function (response) {

            console.log("page-retrieved-successfully ")

            let $tableContent = $("#table-content");
            $tableContent.empty()

            response.content.forEach(function (element) {
                console.log(element)
                $tableContent.append(
                    "<tr class='row'>" +
                    " <td class='cell-info'><strong>" + element.id + "</strong></td>" +
                    " <td class='cell-info'> "+ element.sender.username + "</td>" +
                    " <td class='cell-info'>" + element.recipient.username + "</td>" +
                    " <td class='cell-info'>" + element.transactionDate + "</td>" +
                    " <td class='cell-info'>" + element.amount + "</td>" +
                    "</tr>"
                );
            });
            $("#current-page-value").text(response.pageable.pageNumber + 1);
            enableDisableButton($("#previous-page"), response.first);
            enableDisableButton($("#next-page"), response.last);
        },
        error: function () {
            console.log("error")
        }

    })
}




